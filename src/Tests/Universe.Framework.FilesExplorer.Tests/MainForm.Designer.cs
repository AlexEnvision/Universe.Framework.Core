﻿using Universe.Windows.Forms.Controls.UI;

namespace Universe.Framework.FilesExplorer.Tests
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            menuStrip1 = new System.Windows.Forms.MenuStrip();
            toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            pSettings = new System.Windows.Forms.Panel();
            groupBox2 = new System.Windows.Forms.GroupBox();
            ulvLog = new UniverseListViewControl_UniverseScrollbar();
            groupBox1 = new System.Windows.Forms.GroupBox();
            cbAllowRunAsSystemUser = new System.Windows.Forms.CheckBox();
            tbPassword = new System.Windows.Forms.TextBox();
            tbLogin = new System.Windows.Forms.TextBox();
            btConnect = new System.Windows.Forms.Button();
            label2 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            chLogRecord = new System.Windows.Forms.ColumnHeader();
            pWorkSpace = new System.Windows.Forms.Panel();
            mainPanel = new System.Windows.Forms.SplitContainer();
            browserControlSource = new BrowserControl();
            browserControlDest = new BrowserControl();
            statusStrip1 = new System.Windows.Forms.StatusStrip();
            scMain = new System.Windows.Forms.SplitContainer();
            chLogRecordSecond = new System.Windows.Forms.ColumnHeader();
            chRecord = new System.Windows.Forms.ColumnHeader();
            chRcd = new System.Windows.Forms.ColumnHeader();
            menuStrip1.SuspendLayout();
            pSettings.SuspendLayout();
            groupBox2.SuspendLayout();
            groupBox1.SuspendLayout();
            pWorkSpace.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)mainPanel).BeginInit();
            mainPanel.Panel1.SuspendLayout();
            mainPanel.Panel2.SuspendLayout();
            mainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)scMain).BeginInit();
            scMain.Panel1.SuspendLayout();
            scMain.Panel2.SuspendLayout();
            scMain.SuspendLayout();
            SuspendLayout();
            // 
            // menuStrip1
            // 
            menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { toolStripMenuItem1 });
            menuStrip1.Location = new System.Drawing.Point(0, 0);
            menuStrip1.Name = "menuStrip1";
            menuStrip1.Padding = new System.Windows.Forms.Padding(8, 3, 0, 3);
            menuStrip1.Size = new System.Drawing.Size(1404, 30);
            menuStrip1.TabIndex = 1;
            menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { exitToolStripMenuItem });
            toolStripMenuItem1.Name = "toolStripMenuItem1";
            toolStripMenuItem1.Size = new System.Drawing.Size(114, 24);
            toolStripMenuItem1.Text = "Приложение";
            // 
            // exitToolStripMenuItem
            // 
            exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            exitToolStripMenuItem.Size = new System.Drawing.Size(136, 26);
            exitToolStripMenuItem.Text = "Выход";
            exitToolStripMenuItem.Click += exitToolStripMenuItem_Click;
            // 
            // pSettings
            // 
            pSettings.Controls.Add(groupBox2);
            pSettings.Controls.Add(groupBox1);
            pSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            pSettings.Location = new System.Drawing.Point(0, 0);
            pSettings.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            pSettings.Name = "pSettings";
            pSettings.Size = new System.Drawing.Size(1404, 334);
            pSettings.TabIndex = 2;
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add(ulvLog);
            groupBox2.Location = new System.Drawing.Point(670, 4);
            groupBox2.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            groupBox2.Name = "groupBox2";
            groupBox2.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            groupBox2.Size = new System.Drawing.Size(720, 326);
            groupBox2.TabIndex = 1;
            groupBox2.TabStop = false;
            groupBox2.Text = "Лог";
            // 
            // ulvLog
            // 
            ulvLog.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] { chRcd });
            ulvLog.HideSelection = false;
            ulvLog.Location = new System.Drawing.Point(8, 27);
            ulvLog.Name = "ulvLog";
            ulvLog.Size = new System.Drawing.Size(704, 292);
            ulvLog.TabIndex = 0;
            ulvLog.UseCompatibleStateImageBehavior = false;
            ulvLog.View = System.Windows.Forms.View.Details;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(cbAllowRunAsSystemUser);
            groupBox1.Controls.Add(tbPassword);
            groupBox1.Controls.Add(tbLogin);
            groupBox1.Controls.Add(btConnect);
            groupBox1.Controls.Add(label2);
            groupBox1.Controls.Add(label1);
            groupBox1.Location = new System.Drawing.Point(16, 4);
            groupBox1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            groupBox1.Name = "groupBox1";
            groupBox1.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            groupBox1.Size = new System.Drawing.Size(646, 177);
            groupBox1.TabIndex = 0;
            groupBox1.TabStop = false;
            groupBox1.Text = "Учётные данные";
            // 
            // cbAllowRunAsSystemUser
            // 
            cbAllowRunAsSystemUser.AutoSize = true;
            cbAllowRunAsSystemUser.Location = new System.Drawing.Point(14, 125);
            cbAllowRunAsSystemUser.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            cbAllowRunAsSystemUser.Name = "cbAllowRunAsSystemUser";
            cbAllowRunAsSystemUser.Size = new System.Drawing.Size(298, 24);
            cbAllowRunAsSystemUser.TabIndex = 5;
            cbAllowRunAsSystemUser.Text = "Вход под пользователем приложения";
            cbAllowRunAsSystemUser.UseVisualStyleBackColor = true;
            // 
            // tbPassword
            // 
            tbPassword.Location = new System.Drawing.Point(78, 76);
            tbPassword.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            tbPassword.Name = "tbPassword";
            tbPassword.PasswordChar = '*';
            tbPassword.Size = new System.Drawing.Size(284, 27);
            tbPassword.TabIndex = 4;
            // 
            // tbLogin
            // 
            tbLogin.Location = new System.Drawing.Point(78, 36);
            tbLogin.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            tbLogin.Name = "tbLogin";
            tbLogin.Size = new System.Drawing.Size(284, 27);
            tbLogin.TabIndex = 3;
            // 
            // btConnect
            // 
            btConnect.Location = new System.Drawing.Point(370, 33);
            btConnect.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            btConnect.Name = "btConnect";
            btConnect.Size = new System.Drawing.Size(251, 32);
            btConnect.TabIndex = 2;
            btConnect.Text = "Подключиться";
            btConnect.UseVisualStyleBackColor = true;
            btConnect.Visible = false;
            btConnect.Click += btConnect_Click;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(14, 81);
            label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(65, 20);
            label2.TabIndex = 1;
            label2.Text = "Пароль:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(14, 39);
            label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(55, 20);
            label1.TabIndex = 0;
            label1.Text = "Логин:";
            // 
            // chLogRecord
            // 
            chLogRecord.Text = "Записи лога";
            chLogRecord.Width = 680;
            // 
            // pWorkSpace
            // 
            pWorkSpace.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            pWorkSpace.Controls.Add(mainPanel);
            pWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            pWorkSpace.Location = new System.Drawing.Point(0, 0);
            pWorkSpace.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            pWorkSpace.Name = "pWorkSpace";
            pWorkSpace.Size = new System.Drawing.Size(1404, 662);
            pWorkSpace.TabIndex = 3;
            // 
            // mainPanel
            // 
            mainPanel.Cursor = System.Windows.Forms.Cursors.VSplit;
            mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            mainPanel.Location = new System.Drawing.Point(0, 0);
            mainPanel.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            mainPanel.Name = "mainPanel";
            // 
            // mainPanel.Panel1
            // 
            mainPanel.Panel1.Controls.Add(browserControlSource);
            // 
            // mainPanel.Panel2
            // 
            mainPanel.Panel2.Controls.Add(browserControlDest);
            mainPanel.Size = new System.Drawing.Size(1402, 660);
            mainPanel.SplitterDistance = 704;
            mainPanel.SplitterWidth = 6;
            mainPanel.TabIndex = 4;
            // 
            // browserControlSource
            // 
            browserControlSource.AllowDrop = true;
            browserControlSource.Dock = System.Windows.Forms.DockStyle.Fill;
            browserControlSource.Location = new System.Drawing.Point(0, 0);
            browserControlSource.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            browserControlSource.Name = "browserControlSource";
            browserControlSource.RootFolder = null;
            browserControlSource.Size = new System.Drawing.Size(704, 660);
            browserControlSource.TabIndex = 1;
            // 
            // browserControlDest
            // 
            browserControlDest.AllowDrop = true;
            browserControlDest.Dock = System.Windows.Forms.DockStyle.Fill;
            browserControlDest.Location = new System.Drawing.Point(0, 0);
            browserControlDest.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            browserControlDest.Name = "browserControlDest";
            browserControlDest.RootFolder = null;
            browserControlDest.Size = new System.Drawing.Size(692, 660);
            browserControlDest.TabIndex = 0;
            // 
            // statusStrip1
            // 
            statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            statusStrip1.Location = new System.Drawing.Point(0, 1033);
            statusStrip1.Name = "statusStrip1";
            statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 18, 0);
            statusStrip1.Size = new System.Drawing.Size(1404, 22);
            statusStrip1.TabIndex = 4;
            statusStrip1.Text = "statusStrip1";
            // 
            // scMain
            // 
            scMain.Dock = System.Windows.Forms.DockStyle.Fill;
            scMain.Location = new System.Drawing.Point(0, 30);
            scMain.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            scMain.Name = "scMain";
            scMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scMain.Panel1
            // 
            scMain.Panel1.Controls.Add(pSettings);
            // 
            // scMain.Panel2
            // 
            scMain.Panel2.Controls.Add(pWorkSpace);
            scMain.Size = new System.Drawing.Size(1404, 1003);
            scMain.SplitterDistance = 334;
            scMain.SplitterWidth = 7;
            scMain.TabIndex = 5;
            // 
            // chLogRecordSecond
            // 
            chLogRecordSecond.Text = "Запись лога";
            chLogRecordSecond.Width = 680;
            // 
            // chRecord
            // 
            chRecord.Text = "Запись лога";
            chRecord.Width = 680;
            // 
            // chRcd
            // 
            chRcd.Text = "Запись лога";
            chRcd.Width = 680;
            // 
            // MainForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(1404, 1055);
            Controls.Add(scMain);
            Controls.Add(statusStrip1);
            Controls.Add(menuStrip1);
            MainMenuStrip = menuStrip1;
            Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            Name = "MainForm";
            FormClosing += MainForm_FormClosing;
            Load += MainForm_Load;
            menuStrip1.ResumeLayout(false);
            menuStrip1.PerformLayout();
            pSettings.ResumeLayout(false);
            groupBox2.ResumeLayout(false);
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            pWorkSpace.ResumeLayout(false);
            mainPanel.Panel1.ResumeLayout(false);
            mainPanel.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)mainPanel).EndInit();
            mainPanel.ResumeLayout(false);
            scMain.Panel1.ResumeLayout(false);
            scMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)scMain).EndInit();
            scMain.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Panel pSettings;
        private System.Windows.Forms.Panel pWorkSpace;
        private System.Windows.Forms.SplitContainer mainPanel;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btConnect;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.TextBox tbLogin;
        private System.Windows.Forms.GroupBox groupBox2;
        private BrowserControl browserControlDest;
        private BrowserControl browserControlSource;
        private System.Windows.Forms.SplitContainer scMain;
        private System.Windows.Forms.CheckBox cbAllowRunAsSystemUser;
        private System.Windows.Forms.ColumnHeader chLogRecord;
        private System.Windows.Forms.ColumnHeader chLogRecordSecond;
        private UniverseListViewControl_UniverseScrollbar ulvLog;
        private System.Windows.Forms.ColumnHeader chRecord;
        private System.Windows.Forms.ColumnHeader chRcd;
    }
}

