﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.Framework.Core                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.Framework.Core                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using Universe.Framework.RestApiClientApp.Tests.Base;
using Universe.REST.Adapter;

namespace Universe.Framework.RestApiClientApp.Tests.REST;

public class NugetApiDownloaderTest : ITest
{
    public string SearchSystemName => "https://api.nuget.org/v3-flatcontainer/";

    public string PackagePath => "Universe.Net.Nuget.API/0.9.5.1/Universe.Net.Nuget.API.0.9.5.1.nupkg";

    public void Run()
    {
        var adapter = new DataSourceAdapter(SearchSystemName);
        var amount = 1024;

        var random = new Random();

        for (int i = 0; i < amount; i++)
        {
            Console.Clear();
            Console.WriteLine($"Request number: {i + 1} of package '{PackagePath}'");

            var progress = ((i + 1) / (amount * (1.0))) * 100.0;
            Console.WriteLine($"Progress: {progress} %");

            var response = adapter.CreateGetRequest(SearchSystemName + PackagePath);

            //Console.WriteLine("Content: ");
            if (!response.IsSuccessful)
            {
                Console.WriteLine("Unsuccessful request...");
                Console.ReadLine();
                return;
            }

            //Console.WriteLine(response.ServiceAnswer);
            //Console.WriteLine();

            var coef = random.Next(3, 42);
            var coefIi = random.Next(99, 200);
            var timeout = coef * coefIi;
            Thread.Sleep(timeout);
        }

        Console.ReadLine();
    }
}