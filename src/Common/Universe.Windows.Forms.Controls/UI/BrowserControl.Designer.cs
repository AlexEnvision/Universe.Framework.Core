﻿namespace Universe.Windows.Forms.Controls.UI
{
    partial class BrowserControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            groupBox3 = new System.Windows.Forms.GroupBox();
            tbSelectRootDirectory = new System.Windows.Forms.Button();
            tbStartFolder = new System.Windows.Forms.TextBox();
            scHeaderBoby = new System.Windows.Forms.SplitContainer();
            scBrowser = new System.Windows.Forms.SplitContainer();
            tvFolderBrowser = new TreeFolderBrowserControl();
            lvFileBrowser = new ListViewFileBrowserControl();
            ctxListItemsMenu = new System.Windows.Forms.ContextMenuStrip(components);
            copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            moveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            removeItem = new System.Windows.Forms.ToolStripMenuItem();
            toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)scHeaderBoby).BeginInit();
            scHeaderBoby.Panel1.SuspendLayout();
            scHeaderBoby.Panel2.SuspendLayout();
            scHeaderBoby.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)scBrowser).BeginInit();
            scBrowser.Panel1.SuspendLayout();
            scBrowser.Panel2.SuspendLayout();
            scBrowser.SuspendLayout();
            ctxListItemsMenu.SuspendLayout();
            SuspendLayout();
            // 
            // groupBox3
            // 
            groupBox3.AutoSize = true;
            groupBox3.Controls.Add(tbSelectRootDirectory);
            groupBox3.Controls.Add(tbStartFolder);
            groupBox3.Location = new System.Drawing.Point(11, 9);
            groupBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            groupBox3.Name = "groupBox3";
            groupBox3.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            groupBox3.Size = new System.Drawing.Size(562, 102);
            groupBox3.TabIndex = 3;
            groupBox3.TabStop = false;
            // 
            // tbSelectRootDirectory
            // 
            tbSelectRootDirectory.Location = new System.Drawing.Point(483, 28);
            tbSelectRootDirectory.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            tbSelectRootDirectory.Name = "tbSelectRootDirectory";
            tbSelectRootDirectory.Size = new System.Drawing.Size(71, 42);
            tbSelectRootDirectory.TabIndex = 5;
            tbSelectRootDirectory.Text = "...";
            tbSelectRootDirectory.UseVisualStyleBackColor = true;
            tbSelectRootDirectory.Click += tbSelectRootDirectory_Click;
            // 
            // tbStartFolder
            // 
            tbStartFolder.Location = new System.Drawing.Point(8, 30);
            tbStartFolder.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            tbStartFolder.Multiline = true;
            tbStartFolder.Name = "tbStartFolder";
            tbStartFolder.Size = new System.Drawing.Size(467, 39);
            tbStartFolder.TabIndex = 4;
            // 
            // scHeaderBoby
            // 
            scHeaderBoby.Dock = System.Windows.Forms.DockStyle.Fill;
            scHeaderBoby.Location = new System.Drawing.Point(0, 0);
            scHeaderBoby.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            scHeaderBoby.Name = "scHeaderBoby";
            scHeaderBoby.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scHeaderBoby.Panel1
            // 
            scHeaderBoby.Panel1.Controls.Add(groupBox3);
            // 
            // scHeaderBoby.Panel2
            // 
            scHeaderBoby.Panel2.Controls.Add(scBrowser);
            scHeaderBoby.Size = new System.Drawing.Size(633, 637);
            scHeaderBoby.SplitterDistance = 123;
            scHeaderBoby.SplitterWidth = 6;
            scHeaderBoby.TabIndex = 4;
            // 
            // scBrowser
            // 
            scBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            scBrowser.Location = new System.Drawing.Point(0, 0);
            scBrowser.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            scBrowser.Name = "scBrowser";
            // 
            // scBrowser.Panel1
            // 
            scBrowser.Panel1.Controls.Add(tvFolderBrowser);
            // 
            // scBrowser.Panel2
            // 
            scBrowser.Panel2.Controls.Add(lvFileBrowser);
            scBrowser.Size = new System.Drawing.Size(633, 508);
            scBrowser.SplitterDistance = 298;
            scBrowser.SplitterWidth = 5;
            scBrowser.TabIndex = 0;
            // 
            // tvFolderBrowser
            // 
            tvFolderBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            tvFolderBrowser.Location = new System.Drawing.Point(0, 0);
            tvFolderBrowser.Margin = new System.Windows.Forms.Padding(5, 8, 5, 8);
            tvFolderBrowser.Name = "tvFolderBrowser";
            tvFolderBrowser.Size = new System.Drawing.Size(298, 508);
            tvFolderBrowser.TabIndex = 0;
            // 
            // lvFileBrowser
            // 
            lvFileBrowser.AllowDrop = true;
            lvFileBrowser.ContextMenuStrip = ctxListItemsMenu;
            lvFileBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            lvFileBrowser.Location = new System.Drawing.Point(0, 0);
            lvFileBrowser.Margin = new System.Windows.Forms.Padding(5, 8, 5, 8);
            lvFileBrowser.Name = "lvFileBrowser";
            lvFileBrowser.Size = new System.Drawing.Size(330, 508);
            lvFileBrowser.TabIndex = 0;
            // 
            // ctxListItemsMenu
            // 
            ctxListItemsMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            ctxListItemsMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { copyToolStripMenuItem, moveToolStripMenuItem, removeItem, toolStripSeparator1, pasteToolStripMenuItem });
            ctxListItemsMenu.Name = "ctxItemMenu";
            ctxListItemsMenu.Size = new System.Drawing.Size(170, 106);
            // 
            // copyToolStripMenuItem
            // 
            copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            copyToolStripMenuItem.Size = new System.Drawing.Size(169, 24);
            copyToolStripMenuItem.Text = "Копировать";
            copyToolStripMenuItem.Click += copyToolStripMenuItem_Click;
            // 
            // moveToolStripMenuItem
            // 
            moveToolStripMenuItem.Name = "moveToolStripMenuItem";
            moveToolStripMenuItem.Size = new System.Drawing.Size(169, 24);
            moveToolStripMenuItem.Text = "Переместить";
            moveToolStripMenuItem.Click += moveToolStripMenuItem_Click;
            // 
            // removeItem
            // 
            removeItem.Name = "removeItem";
            removeItem.Size = new System.Drawing.Size(169, 24);
            removeItem.Text = "Удалить";
            removeItem.Click += removeItem_Click;
            // 
            // toolStripSeparator1
            // 
            toolStripSeparator1.Name = "toolStripSeparator1";
            toolStripSeparator1.Size = new System.Drawing.Size(166, 6);
            // 
            // pasteToolStripMenuItem
            // 
            pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            pasteToolStripMenuItem.Size = new System.Drawing.Size(169, 24);
            pasteToolStripMenuItem.Text = "Вставить";
            pasteToolStripMenuItem.Click += pasteToolStripMenuItem_Click;
            // 
            // BrowserControl
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(scHeaderBoby);
            Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            Name = "BrowserControl";
            Size = new System.Drawing.Size(633, 637);
            groupBox3.ResumeLayout(false);
            groupBox3.PerformLayout();
            scHeaderBoby.Panel1.ResumeLayout(false);
            scHeaderBoby.Panel1.PerformLayout();
            scHeaderBoby.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)scHeaderBoby).EndInit();
            scHeaderBoby.ResumeLayout(false);
            scBrowser.Panel1.ResumeLayout(false);
            scBrowser.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)scBrowser).EndInit();
            scBrowser.ResumeLayout(false);
            ctxListItemsMenu.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button tbSelectRootDirectory;
        private System.Windows.Forms.TextBox tbStartFolder;
        private System.Windows.Forms.SplitContainer scHeaderBoby;
        private System.Windows.Forms.SplitContainer scBrowser;
        private TreeFolderBrowserControl tvFolderBrowser;
        private ListViewFileBrowserControl lvFileBrowser;
        private System.Windows.Forms.ContextMenuStrip ctxListItemsMenu;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moveToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeItem;
    }
}
