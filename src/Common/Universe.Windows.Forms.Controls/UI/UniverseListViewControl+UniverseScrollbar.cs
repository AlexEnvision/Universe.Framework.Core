﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Universe.Windows.Forms.Controls.UI.Scrollbar;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using ListView = System.Windows.Forms.ListView;

namespace Universe.Windows.Forms.Controls.UI
{
    public partial class UniverseListViewControl_UniverseScrollbar : UniverseListViewControl
    {
        public virtual Scrollbar.UniverseScrollbar UsVerticalScrollbar
        {
            get => usVerticalScrollbar;
            set => usVerticalScrollbar = value;
        }

        public UniverseListViewControl_UniverseScrollbar()
        {
            InitializeComponent();

            DefaultScrollbarStyleHelper.ApplyStyle(usVerticalScrollbar, DefaultScrollbarStyleHelper.StyleTypeEnum.Blue);
            this.VScrollbar = new ScrollbarCollector(usVerticalScrollbar);

            ulvMain.Visible = false;
        }
    }
}