﻿namespace Universe.Windows.Forms.Controls.UI
{
    partial class ListViewFileBrowserControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListViewFileBrowserControl));
            lvMain = new System.Windows.Forms.ListView();
            FileFolder = new System.Windows.Forms.ImageList(components);
            SuspendLayout();
            // 
            // lvMain
            // 
            lvMain.Dock = System.Windows.Forms.DockStyle.Fill;
            lvMain.HideSelection = false;
            lvMain.LargeImageList = FileFolder;
            lvMain.Location = new System.Drawing.Point(0, 0);
            lvMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            lvMain.Name = "lvMain";
            lvMain.Size = new System.Drawing.Size(287, 423);
            lvMain.SmallImageList = FileFolder;
            lvMain.TabIndex = 0;
            lvMain.UseCompatibleStateImageBehavior = false;
            // 
            // FileFolder
            // 
            FileFolder.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            FileFolder.ImageStream = (System.Windows.Forms.ImageListStreamer)resources.GetObject("FileFolder.ImageStream");
            FileFolder.TransparentColor = System.Drawing.Color.Transparent;
            FileFolder.Images.SetKeyName(0, "");
            FileFolder.Images.SetKeyName(1, "");
            // 
            // ListViewFileBrowserControl
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(lvMain);
            Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            Name = "ListViewFileBrowserControl";
            Size = new System.Drawing.Size(287, 423);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.ListView lvMain;
        private System.Windows.Forms.ImageList FileFolder;
    }
}
