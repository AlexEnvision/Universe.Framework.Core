﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.Framework.Core                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.Framework.Core                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Universe.Windows.Forms.Controls.UI
{
    /// <summary>
    ///     A special label for media players
    /// </summary>
    public class MediaLabeControl : Label
    {
        public const int DefaultPageCount = 25;

        public const int DefaultInterval = 3000;

        public int PageCount { get; set; }

        public int Interval
        {
            get
            {
                if (_interval == 0 || _interval == 100)
                    return DefaultInterval;

                return _interval;
            }
            set
            {
                if (_mTimer != null) 
                    _mTimer.Interval = value;

                _interval = value;
            }
        }

        private PagedText TextPages { get; set; }

        private readonly Timer? _mTimer;

        private int _interval;

        /// <summary>
        ///     Initializes an instance of the class <see cref="MediaLabeControl"/>
        /// </summary>
        public MediaLabeControl()
        {
            PageCount = DefaultPageCount;
            Interval = DefaultInterval;

            _mTimer = new Timer();
            _mTimer.Enabled = true;

            _mTimer.Interval = Interval;

            _mTimer.Tick += timer_tick;

            TextPages = new PagedText();
        }

        private void timer_tick(object? sender, EventArgs e)
        {
            for (var index = 0; index < TextPages.Pages.Count; index++)
            {
                var textPage = TextPages.Pages[index];
                if (TextPages.CurrentPage != null && 
                    textPage.PageNumber == TextPages.CurrentPage.PageNumber)
                {
                    if (index < TextPages.Pages.Count - 1)
                    {
                        TextPages.CurrentPage = TextPages.Pages[index + 1];
                        this.SafeCall(() => base.Text = TextPages.CurrentPage?.Text ?? string.Empty);
                    }
                    else
                    {
                        TextPages.CurrentPage = TextPages.Pages[0];
                        this.SafeCall(() => base.Text = TextPages.CurrentPage?.Text ?? string.Empty);
                    }
                    break;
                }
            }
        }

        public void ToPageText(string text)
        {
            List<Page> parts = new List<Page>();

            var number = 1;
            for (int i = 0; i < text.Length; i += PageCount)
            {
                var part = text.Skip(i).Take(PageCount).ToArray();
                var pText = i == 0 ? new string(part) : "..." + new string(part);
                pText = i < text.Length - PageCount ? pText + "..." : pText;

                Page model = new Page(PageCount)
                {
                    PageNumber = number,
                    Text = pText
                };

                parts.Add(model);
                number++;
            }

            TextPages = new PagedText
            {
                Pages = parts,
                CurrentPage = parts.FirstOrDefault()
            };

            Text = TextPages.CurrentPage?.Text ?? string.Empty;
        }

        /// <summary>
        ///     Paginated the Text property
        /// </summary>
        public class PagedText
        {
            /// <summary>
            ///     Pages, that we get when we split a string
            /// </summary>
            public List<Page> Pages { get; set; }

            /// <summary>
            ///     The current page
            /// </summary>
            public Page? CurrentPage { get; set; }

            public PagedText()
            {
                Pages = new List<Page>();
            }
        }

        /// <summary>
        ///     Page with a text
        /// </summary>
        public class Page
        {
            /// <summary>
            ///     The length of page
            /// </summary>
            public int PageCount { get; set; }

            /// <summary>
            ///     The number of page
            /// </summary>
            public int PageNumber { get; set; }

            /// <summary>
            ///     The text of the page
            /// </summary>
            public string Text { get; set; }

            /// <summary>
            ///     Initializes an an instance of class <see cref="Page"/>
            /// </summary>
            public Page(int pageCount)
            {
                PageCount = pageCount;
                Text = string.Empty;
            }
        }
    }
}