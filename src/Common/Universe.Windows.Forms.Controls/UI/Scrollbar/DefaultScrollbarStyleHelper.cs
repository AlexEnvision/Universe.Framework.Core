﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.Framework.Core                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.Framework.Core                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System.Drawing;
using Universe.Windows.Forms.Controls.UI.Scrollbar.BasePainters;

namespace Universe.Windows.Forms.Controls.UI.Scrollbar
{
    public static class DefaultScrollbarStyleHelper
    {
        public enum StyleTypeEnum
        {
            Default,
            Black,
            Blue,
            Progressive
        }

        public static void ApplyStyle(UniverseScrollbar scrollbar, StyleTypeEnum styleType)
        {
            if (styleType == StyleTypeEnum.Default)
            {
                scrollbar.SetCustomBackBrush(null, null);

                scrollbar.SetUpperButtonPainter(
                    new StackedPainters(
                        new WindowsStyledButtonPainter(),
                        new PainterFilterSize(
                            new SymbolPainter(SymbolPainter.SymbolEnum.TriangleUp, true, 1, Color.Black, Color.Black, Color.Black),
                            PainterFilterSize.Alignment.Center, PainterFilterSize.Alignment.Center, 0, 0, 0, 0, 10, 2)));

                scrollbar.SetLowerButtonPainter(
                    new StackedPainters(
                        new WindowsStyledButtonPainter(),
                        new PainterFilterSize(
                            new SymbolPainter(SymbolPainter.SymbolEnum.TriangleDown, true, 1, Color.Black, Color.Black, Color.Black),
                            PainterFilterSize.Alignment.Center, PainterFilterSize.Alignment.Center, 0, 0, 0, 0, 10, 2)));

                scrollbar.SetSmallThumbPainter(new WindowsStyledButtonPainter());

                scrollbar.SetLargeThumbPainter(
                    new StackedPainters(
                        new WindowsStyledButtonPainter(),
                        new PainterFilterSize(
                            new SymbolPainter(SymbolPainter.SymbolEnum.GripH, true, 1, Color.Black, Color.Black, Color.Black),
                            PainterFilterSize.Alignment.Center, PainterFilterSize.Alignment.Center, 0, 0, 0, 0, 10, 2)
                            ));
            }
            else if (styleType == StyleTypeEnum.Black)
            {
                scrollbar.SetCustomBackBrush(null, null);
                scrollbar.SetUpperButtonPainter(
                    new StackedPainters(
                        new PainterFilterNoText(new Office2007BlackButtonPainter()),
                        new PainterFilterSize(
                            new SymbolPainter(SymbolPainter.SymbolEnum.TriangleUp, true, 1, Color.Black, Color.Black, Color.Black),
                            PainterFilterSize.Alignment.Center, PainterFilterSize.Alignment.Center, 0, 0, 0, 0, 10, 2)));

                scrollbar.SetLowerButtonPainter(
                    new StackedPainters(
                        new PainterFilterNoText(new Office2007BlackButtonPainter()),
                        new PainterFilterSize(
                            new SymbolPainter(SymbolPainter.SymbolEnum.TriangleDown, true, 1, Color.Black, Color.Black, Color.Black),
                            PainterFilterSize.Alignment.Center, PainterFilterSize.Alignment.Center, 0, 0, 0, 0, 10, 2)));

                scrollbar.SetSmallThumbPainter(new PainterFilterNoText(new Office2007BlackButtonPainter()));

                scrollbar.SetLargeThumbPainter(
                    new StackedPainters(
                        new PainterFilterNoText(new Office2007BlackButtonPainter()),
                        new PainterFilterSize(
                            new SymbolPainter(SymbolPainter.SymbolEnum.GripH, true, 1, Color.Black, Color.Black, Color.Black),
                            PainterFilterSize.Alignment.Center, PainterFilterSize.Alignment.Center, 0, 0, 0, 0, 10, 2)
                            ));
            }
            else if (styleType == StyleTypeEnum.Blue)
            {
                scrollbar.SetCustomBackBrush(null, null);
                scrollbar.SetUpperButtonPainter(
                    new StackedPainters(
                        new PainterFilterNoText(new Office2007BlueButtonPainter()),
                        new PainterFilterSize(
                            new SymbolPainter(SymbolPainter.SymbolEnum.TriangleUp, true, 1, Color.Black, Color.Black, Color.Black),
                            PainterFilterSize.Alignment.Center, PainterFilterSize.Alignment.Center, 0, 0, 0, 0, 10, 2)));

                scrollbar.SetLowerButtonPainter(
                    new StackedPainters(
                        new PainterFilterNoText(new Office2007BlueButtonPainter()),
                        new PainterFilterSize(
                            new SymbolPainter(SymbolPainter.SymbolEnum.TriangleDown, true, 1, Color.Black, Color.Black, Color.Black),
                            PainterFilterSize.Alignment.Center, PainterFilterSize.Alignment.Center, 0, 0, 0, 0, 10, 2)));

                scrollbar.SetSmallThumbPainter(new PainterFilterNoText(new Office2007BlueButtonPainter()));

                scrollbar.SetLargeThumbPainter(
                    new StackedPainters(
                        new PainterFilterNoText(new Office2007BlueButtonPainter()),
                        new PainterFilterSize(
                            new SymbolPainter(SymbolPainter.SymbolEnum.GripH, true, 1, Color.Black, Color.Black, Color.Black),
                            PainterFilterSize.Alignment.Center, PainterFilterSize.Alignment.Center, 0, 0, 0, 0, 10, 2)
                            ));
            }
            else if (styleType == StyleTypeEnum.Progressive)
            {
                scrollbar.SetCustomBackBrush(null, null);
                scrollbar.SetUpperButtonPainter(
                    new StackedPainters(
                        new PainterFilterNoText(new ProgressiveButtonReversedPainter()),
                        new PainterFilterSize(
                            new SymbolPainter(SymbolPainter.SymbolEnum.TriangleUp, true, 1, Color.Black, Color.Black, Color.Black),
                            PainterFilterSize.Alignment.Center, PainterFilterSize.Alignment.Center, 0, 0, 0, 0, 10, 2)));

                scrollbar.SetLowerButtonPainter(
                    new StackedPainters(
                        new PainterFilterNoText(new ProgressiveButtonPainter()),
                        new PainterFilterSize(
                            new SymbolPainter(SymbolPainter.SymbolEnum.TriangleDown, true, 1, Color.Black, Color.Black, Color.Black),
                            PainterFilterSize.Alignment.Center, PainterFilterSize.Alignment.Center, 0, 0, 0, 0, 10, 2)));

                scrollbar.SetSmallThumbPainter(new PainterFilterNoText(new Office2007BlueButtonPainter()));

                scrollbar.SetLargeThumbPainter(
                    new StackedPainters(
                        new PainterFilterNoText(new Office2007BlueButtonPainter()),
                        new PainterFilterSize(
                            new SymbolPainter(SymbolPainter.SymbolEnum.GripH, true, 1, Color.Black, Color.Black, Color.Black),
                            PainterFilterSize.Alignment.Center, PainterFilterSize.Alignment.Center, 0, 0, 0, 0, 10, 2)
                    ));
            }
        }
    }
}