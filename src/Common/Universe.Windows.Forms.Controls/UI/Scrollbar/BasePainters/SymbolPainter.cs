﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.Framework.Core                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.Framework.Core                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Universe.Windows.Forms.Controls.UI.Scrollbar.BasePainters
{
    public class SymbolPainter : Painter
    {
        public static Painter Create(Painter backgroundPainter, SymbolEnum symbol, bool fill, int penWidth, Color forecolor, Color hoverColor, Color clickColor)
        {
            return new StackedPainters(
                new PainterFilterNoText(backgroundPainter),
                new SymbolPainter(symbol, fill, penWidth, forecolor, hoverColor, clickColor));
        }

        public enum SymbolEnum
        {
            TriangleDown,
            TriangleUp,
            GripH
        }

        private bool _fill;
        private SymbolEnum _symbol;
        private int _penWidth;
        private Color _color;
        private Pen _pen;
        private Brush _fillBrush;
        private Pen _hoverPen;
        private Pen _clickPen;

        private List<SymbolEnum> _noFill = new List<SymbolEnum>(new SymbolEnum[]{
            SymbolEnum.GripH
        });
            
        public SymbolPainter(SymbolEnum symbol, bool fill, int penWidth, Color forecolor, Color hoverPen, Color clickPen)
        {
            _symbol = symbol;
            _fill = fill;
            _penWidth = penWidth;
            _color = forecolor;
            _hoverPen = new Pen(hoverPen, penWidth);
            _clickPen = new Pen(clickPen, penWidth);
            _pen = new Pen(_color, penWidth);
            _fillBrush = new SolidBrush(forecolor);

            

        }

        private GraphicsPath BuildTriangleDown(Rectangle bounds)
        {
            int xPadding = bounds.Width/10;
            int yPadding = bounds.Height/10;
            int triangleHalf = Math.Max(0, bounds.Width /2 - xPadding);

            GraphicsPath triangle = new GraphicsPath();
            triangle.AddLine(bounds.Left + xPadding, bounds.Top + yPadding, bounds.Left + xPadding + 2 * triangleHalf, bounds.Top + yPadding);
            triangle.AddLine(bounds.Left + xPadding + 2 * triangleHalf, bounds.Top + yPadding, bounds.Left + xPadding + triangleHalf, bounds.Bottom - yPadding);
            triangle.CloseAllFigures();
            return triangle;
        }

        private GraphicsPath BuildTriangleUp(Rectangle bounds)
        {
            int xPadding = bounds.Width / 10;
            int yPadding = bounds.Height / 10;
            int triangleHalf = Math.Max(0, bounds.Width / 2 - xPadding);

            GraphicsPath triangle = new GraphicsPath();
            triangle.AddLine(bounds.Left + xPadding, bounds.Bottom - yPadding, bounds.Left + xPadding + 2 * triangleHalf, bounds.Bottom - yPadding);
            triangle.AddLine(bounds.Left + xPadding + 2 * triangleHalf, bounds.Bottom - yPadding, bounds.Left + xPadding + triangleHalf, bounds.Top + yPadding);
            triangle.CloseAllFigures();
            return triangle;
        }

        private GraphicsPath BuildGripH(Rectangle bounds)
        {
            int xPadding = bounds.Width / 10;
            int yPadding = bounds.Height / 10;
            

            int half = (int)((double)(bounds.Height - 2*yPadding) / 2.0);

            GraphicsPath grip = new GraphicsPath();
            grip.AddLine(bounds.Left + xPadding, bounds.Top + yPadding, bounds.Right - xPadding, bounds.Top + yPadding);
            grip.StartFigure();
            grip.AddLine(bounds.Left + xPadding, bounds.Top + yPadding + half, bounds.Right - xPadding, bounds.Top + yPadding+ half);
            grip.StartFigure();
            grip.AddLine(bounds.Left + xPadding, bounds.Top + yPadding + 2*half, bounds.Right - xPadding, bounds.Top + yPadding + 2*half);

            return grip;
        }

        public override void Paint(Graphics g, Rectangle position, Painter.State buttonState, string text, Image buttonImage, Font textFont, Rectangle? referencePosition)
        {
            GraphicsPath path;

            Pen myPen = _pen;

            if (buttonState == State.Hover)
                myPen = _hoverPen;
            else if (buttonState == State.Pressed)
                myPen = _clickPen;

            if (_symbol == SymbolEnum.TriangleDown)
                path = BuildTriangleDown(position);
            else if (_symbol == SymbolEnum.TriangleUp)
                path = BuildTriangleUp(position);
            else if (_symbol == SymbolEnum.GripH)
                path = BuildGripH(position);
            else
                throw new NotImplementedException("Symbol not implemented");

            if (_fill && _noFill.Contains(_symbol) == false)
            {
                g.FillPath(_fillBrush, path);

                if (buttonState == State.Hover)
                    g.DrawPath(myPen, path);
                else if (buttonState == State.Pressed)
                    g.DrawPath(myPen, path);
            }
            else
                g.DrawPath(myPen, path);
        }
    }
}
