﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.Framework.Core                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.Framework.Core                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Drawing;

namespace Universe.Windows.Forms.Controls.UI.Scrollbar.BasePainters
{
    public abstract class Painter
    {
        public enum State
        {
            Normal,
            Pressed,
            Hover
        }

        public abstract void Paint(Graphics g, Rectangle position, State buttonState, string text, Image buttonImage, Font textFont, Rectangle? referencePosition);
    }

    public abstract class DoubleBrushPainter : Painter
    {
        

        protected abstract Color BorderColor(Painter.State state);
        protected abstract int BorderWidth(Painter.State state);
        protected abstract Brush UpperBrush(Painter.State state, Rectangle bounds);
        protected abstract Brush LowerBrush(Painter.State state, Rectangle bounds);
        protected abstract Color FontColor(Painter.State state);

        public override void Paint(Graphics g, Rectangle position, Painter.State buttonState, string text, Image buttonImage, Font textFont, Rectangle? referencePosition)
        {
            Rectangle upperRect;
            Rectangle lowerRect;

            if (buttonState != State.Pressed)
            {
                upperRect = new Rectangle(position.Left, position.Top, position.Width, position.Height / 2 - position.Height / 8);
                lowerRect = new Rectangle(position.Left, position.Top + position.Height / 2 - position.Height / 8, position.Width, position.Height / 2 + position.Height / 8);
            }
            else
            {
                upperRect = new Rectangle(position.Left, position.Top, position.Width, Math.Max(1, position.Height / 2));
                lowerRect = new Rectangle(position.Left, position.Top + position.Height / 2 , position.Width, Math.Max(1,position.Height / 2) );
            }

            g.FillRectangle(UpperBrush(buttonState, upperRect), upperRect);
            g.FillRectangle(LowerBrush(buttonState, lowerRect), lowerRect);

            Rectangle borderRect = new Rectangle(position.X, position.Y, position.Width - BorderWidth(buttonState), position.Height - BorderWidth(buttonState));

            g.DrawRectangle(new Pen(BorderColor(buttonState), BorderWidth(buttonState)), borderRect);


            Rectangle textBounds;

            if (buttonImage == null)
                textBounds = new Rectangle(
                   position.X + 2,
                   position.Y + 2,
                   position.Width - 4,
                   position.Height - 4);
            else
            {
                //There is an image, calculate the the width from te max height
                int imageHeight = position.Height - 10;

                double imageRatio = (double)buttonImage.Width / (double)buttonImage.Height;
                int imageWidth = (int)(imageRatio * (double)imageHeight);

                Rectangle imagePosition = new Rectangle(position.X + 5, position.Y + 5, imageWidth, imageHeight);
                textBounds = new Rectangle(imagePosition.Right + 2, position.Y + 2, position.Width - imagePosition.Width - 10, position.Height - 4);

                g.DrawImage(buttonImage, imagePosition, new Rectangle(0, 0, buttonImage.Width, buttonImage.Height), GraphicsUnit.Pixel);
            }

            StringFormat format = new StringFormat();
            format.LineAlignment = StringAlignment.Center;
            format.Alignment = StringAlignment.Center;
            format.FormatFlags = StringFormatFlags.NoClip
            | StringFormatFlags.FitBlackBox | StringFormatFlags.NoWrap;


            if (referencePosition != null)
            {

                double xRatio = (double)position.Width / (double)referencePosition.Value.Width;
                double yRatio = (double)position.Height / (double)referencePosition.Value.Height;
                textFont = ScaledFont(textFont, xRatio, yRatio, text, textBounds, g, format);
            }


            //TextRenderer.DrawText(g, button.ButtonText, myFont, textBounds, FontColor(myState), Color.Transparent, TextFormatFlags.NoPadding | TextFormatFlags.VerticalCenter | TextFormatFlags.HorizontalCenter);
            using (Brush frontBrush = new SolidBrush(FontColor(buttonState)))
            {
                //g.FillPath(frontBrush, GeneratePath(button.ButtonText, textBounds, myFont.Style));
                g.DrawString(text, textFont, frontBrush,
                    textBounds, format);
            }

        }

        protected Font ScaledFont(Font referenceFont, double xRatio, double yRatio, string text, Rectangle fitTo, Graphics g, StringFormat format)
        {
            //Calculate scaled fonts based on the y-scale-factor
            float fontsize_YScaled = Math.Max(1, (int)(referenceFont.Size * yRatio));

            Font newFont = new Font(referenceFont.FontFamily, fontsize_YScaled, referenceFont.Style);
            //Check if text with y-scaled font fits
            SizeF textSize = g.MeasureString(text, newFont, fitTo.Width, format);

            if (textSize.Height <= fitTo.Height)
                return newFont;
            else
            {
                do
                {
                    newFont.Dispose();
                    newFont = null;

                    if (fontsize_YScaled <= 1)
                        return new Font(FontFamily.GenericSansSerif, 1, referenceFont.Style);

                    fontsize_YScaled -= 0.5f;
                    newFont = new Font(referenceFont.FontFamily, fontsize_YScaled, referenceFont.Style);
                    //And check again
                    textSize = g.MeasureString(text, newFont, fitTo.Width);

                    if (textSize.Height <= fitTo.Height)
                        return newFont;


                } while (textSize.Height <= fitTo.Height);
                return newFont;
            }



        }
    }

  
}
