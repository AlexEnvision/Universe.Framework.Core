﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.Framework.Core                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.Framework.Core                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Drawing;

namespace Universe.Windows.Forms.Controls.UI.Scrollbar.BasePainters
{
    public class PainterFilterSize : Painter
    {
        public enum Alignment
        {
            Center,
            Near,
            Far
        }

        private Alignment _hAlign;
        private Alignment _vAlign;
        private int _paddingTop;
        private int _paddingRight;
        private int _paddingLeft;
        private int _paddingBottom;
        private int _maxWidth;

        // width/height = t
        private double _targetRatio;

        private Painter _subPainter;

        public PainterFilterSize(Painter subPainter, Alignment hAlign, Alignment vAlign, 
            int paddingTop, int paddingLeft, int paddingRight, int paddingBottom, 
            int maxWidth,
            double targetRatio)
        {
            _hAlign = hAlign;
            _vAlign = vAlign;
            _paddingTop = paddingTop;
            _paddingBottom = paddingBottom;
            _paddingLeft = paddingLeft;
            _paddingRight = paddingRight;

            _maxWidth = maxWidth;
            _targetRatio = targetRatio;
            _subPainter = subPainter;
        }
        public override void Paint(Graphics g, Rectangle position, Painter.State buttonState, string text, Image buttonImage, Font textFont, Rectangle? referencePosition)
        {
            Rectangle layoutArea = Rectangle.FromLTRB(position.X + _paddingLeft,
                position.Y + _paddingTop,
                position.Right - _paddingRight,
                position.Bottom - _paddingBottom);

            if (layoutArea.Width <= 0 || layoutArea.Height <= 0)
                return;

            double layoutRatio = (double)layoutArea.Width / (double)layoutArea.Height;

            Rectangle targetRect;
            //maximize width
            if (layoutRatio < _targetRatio)
            {   
                int targetWidth = layoutArea.Width;

                if(_maxWidth > 0)
                    targetWidth = Math.Min(layoutArea.Width, _maxWidth);

                int targetHeight = (int)((double)targetWidth / _targetRatio);

                targetRect = new Rectangle(layoutArea.X, layoutArea.Y, targetWidth, targetHeight);

                
            }
            //maximize height
            else
            {
                int targetWidth = (int)((double)layoutArea.Height * _targetRatio);
                int targetHeight = layoutArea.Height;

                if (_maxWidth > 0 && targetWidth > _maxWidth)
                {
                    targetWidth = _maxWidth;
                    targetHeight = (int)((double)targetHeight / _targetRatio);
                }

                targetRect = new Rectangle(layoutArea.X, layoutArea.Y, targetWidth, targetHeight);

                
            }

            if (_vAlign == Alignment.Far)
                targetRect = new Rectangle(targetRect.X, layoutArea.Bottom - targetRect.Height, targetRect.Width, targetRect.Height);
            else if(_vAlign == Alignment.Center)
                targetRect = new Rectangle(targetRect.X, layoutArea.Top + (int)((double)layoutArea.Height / 2.0d - (double)targetRect.Height / 2.0d),
                    targetRect.Width, targetRect.Height);

            if (_hAlign == Alignment.Far)
                targetRect = new Rectangle(layoutArea.Right - targetRect.Width, targetRect.Y, targetRect.Width, targetRect.Height);
            else if(_hAlign == Alignment.Center)
                targetRect = new Rectangle(targetRect.X + (int)((double)layoutArea.Width / 2.0d - (double)targetRect.Width / 2.0d),
                    targetRect.Y, targetRect.Width, targetRect.Height);

            _subPainter.Paint(g, targetRect, buttonState, text, buttonImage, textFont, referencePosition);
        }
    }
}
