﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.Framework.Core                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.Framework.Core                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System.Drawing;
using System.Drawing.Drawing2D;

namespace Universe.Windows.Forms.Controls.UI.Scrollbar.BasePainters
{
    public class Office2007BlackButtonPainter : DoubleBrushPainter
    {

        protected override Color BorderColor(Painter.State state)
        {
            if (state == State.Pressed)
                return Color.FromArgb(0x8b, 0x76, 0x54);
            else
                return Color.FromArgb(0x96, 0x9f, 0xa3);
        }

        protected override Color FontColor(Painter.State state)
        {
            return Color.FromArgb(0x46, 0x46, 0x46);
        }

        protected override int BorderWidth(Painter.State state)
        {
            return 1;
        }

        protected override Brush UpperBrush(Painter.State state, Rectangle bounds)
        {
            if (state == State.Normal)
                return new LinearGradientBrush(bounds, Color.FromArgb(0xd6, 0xde, 0xdf), Color.FromArgb(0xdb, 0xe2, 0xe4), LinearGradientMode.Vertical);
            else if (state == State.Hover)
                return new LinearGradientBrush(bounds, Color.FromArgb(0xfe, 0xfa, 0xe5), Color.FromArgb(0xfb, 0xe0, 0x91), LinearGradientMode.Vertical);
            else
                return new LinearGradientBrush(bounds, Color.FromArgb(0xcc, 0x96, 0x66), Color.FromArgb(0xff, 0xaa, 0x46), LinearGradientMode.Vertical);
        }

        protected override Brush LowerBrush(Painter.State state, Rectangle bounds)
        {
            if (state == State.Normal)
                return new LinearGradientBrush(bounds, Color.FromArgb(0xce, 0xd5, 0xd7), Color.FromArgb(0xdf, 0xe4, 0xe6), LinearGradientMode.Vertical);
            else if (state == State.Hover)
                return new LinearGradientBrush(bounds, Color.FromArgb(0xfe, 0xd2, 0x53), Color.FromArgb(0xff, 0xe3, 0x97), LinearGradientMode.Vertical);
            else
                return new LinearGradientBrush(bounds, Color.FromArgb(0xff, 0x9c, 0x26), Color.FromArgb(0xff, 0xc0, 0x4b), LinearGradientMode.Vertical);

        }
    }

    public class Office2007BlueButtonPainter : Office2007BlackButtonPainter
    {
        protected override Brush UpperBrush(State state, Rectangle bounds)
        {
            if(state == State.Normal)
                return new LinearGradientBrush(bounds, Color.FromArgb(0xc8, 0xdb, 0xef), Color.FromArgb(0xc6, 0xda, 0xf3), LinearGradientMode.Vertical);
            else
                return base.UpperBrush(state, bounds);
        }

        protected override Brush LowerBrush(State state, Rectangle bounds)
        {
            if (state == State.Normal)
                return new LinearGradientBrush(bounds, Color.FromArgb(0xbd, 0xd1, 0xea), Color.FromArgb(0xce, 0xdf, 0xf5), LinearGradientMode.Vertical);
            else
                return base.LowerBrush(state, bounds);
        }
    }

    public class ProgressiveButtonPainter : Office2007BlackButtonPainter
    {
        protected override Brush UpperBrush(State state, Rectangle bounds)
        {
            if (state == State.Normal)
                return new LinearGradientBrush(bounds, Color.FromArgb(0xbd, 0xd1, 0xea), Color.FromArgb(0xce, 0xdf, 0xf5), LinearGradientMode.Vertical);
            else
                return base.UpperBrush(state, bounds);
        }

        protected override Brush LowerBrush(State state, Rectangle bounds)
        {
            Color progressiveBackColor = Color.FromArgb(1, 7, 51);
            Color progressiveTextColor = Color.FromArgb(78, 206, 215);

            if (state == State.Normal)
                return new LinearGradientBrush(bounds, progressiveTextColor, progressiveBackColor, LinearGradientMode.Vertical);
            else
                return base.LowerBrush(state, bounds);
        }
    }

    public class ProgressiveButtonReversedPainter : Office2007BlackButtonPainter
    {
        protected override Brush UpperBrush(State state, Rectangle bounds)
        {
            Color progressiveBackColor = Color.FromArgb(1, 7, 51);
            Color progressiveTextColor = Color.FromArgb(78, 206, 215);

            if (state == State.Normal)
                return new LinearGradientBrush(bounds,  progressiveBackColor, progressiveTextColor, LinearGradientMode.Vertical);
            else
                return base.UpperBrush(state, bounds);
        }

        protected override Brush LowerBrush(State state, Rectangle bounds)
        {
            if (state == State.Normal)
                return new LinearGradientBrush(bounds, Color.FromArgb(0xbd, 0xd1, 0xea), Color.FromArgb(0xce, 0xdf, 0xf5), LinearGradientMode.Vertical);
            else
                return base.LowerBrush(state, bounds);
        }
    }
}
