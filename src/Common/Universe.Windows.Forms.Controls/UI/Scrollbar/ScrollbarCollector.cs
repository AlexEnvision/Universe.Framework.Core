﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.Framework.Core                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.Framework.Core                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System.Collections.Generic;

namespace Universe.Windows.Forms.Controls.UI.Scrollbar
{
    public class ScrollbarCollector : IUniverseScrollbar
    {
        private List<IUniverseScrollbar> _attachedScrollbars = new List<IUniverseScrollbar>();
        private bool _disableChangeEvents = false;

        public ScrollbarCollector(params IUniverseScrollbar[] attachedScrollbars)
        {
            _attachedScrollbars.AddRange(attachedScrollbars);

            foreach(IUniverseScrollbar scrollbar in _attachedScrollbars)
                scrollbar.ValueChanged += new ScrollValueChangedDelegate(scrollbar_ValueChanged);

        }

        private void scrollbar_ValueChanged(IUniverseScrollbar sender, int newValue)
        {
            if (_disableChangeEvents) return;

            _disableChangeEvents = true;
            foreach (IUniverseScrollbar scrollbar in _attachedScrollbars)
            {
                if (scrollbar != sender)
                    scrollbar.Value = newValue;
            }
            _disableChangeEvents = false;
            _value = newValue;
            if (ValueChanged != null)
                ValueChanged(this, newValue);
        }
        #region ICustomScrollbar Members

        public event ScrollValueChangedDelegate ValueChanged;
        

        private int _largeChange = 10;
        public int LargeChange
        {
            get { return _largeChange; }
            set
            {
                _largeChange = value;
                foreach (IUniverseScrollbar attachedScrollbar in _attachedScrollbars)
                    attachedScrollbar.LargeChange = value;
            }
        }

        private int _smallChange = 1;
        public int SmallChange
        {
            get { return _smallChange; }
            set
            {
                _smallChange = value;
                foreach (IUniverseScrollbar attachedScrollbar in _attachedScrollbars)
                    attachedScrollbar.SmallChange = value;
            }
        }

        private int _maximum = 99;
        public int Maximum
        {
            get { return _maximum; }
            set
            {
                _maximum = value;
                foreach (IUniverseScrollbar attachedScrollbar in _attachedScrollbars)
                    attachedScrollbar.Maximum = value;
            }
        }

        private int _minimum = 0;
        public int Minimum
        {
            get { return _minimum; }
            set
            {
                _minimum = value;
                foreach (IUniverseScrollbar attachedScrollbar in _attachedScrollbars)
                    attachedScrollbar.Minimum = value;
            }
        }

        private int _value;
        public int Value
        {
            get { return _value; }
            set
            {
                _value = value;
                foreach (IUniverseScrollbar attachedScrollbar in _attachedScrollbars)
                    attachedScrollbar.Value = value;
            }
        }

        #endregion
    }
}
