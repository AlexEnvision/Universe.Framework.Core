﻿namespace Universe.Windows.Forms.Controls.UI
{
    partial class UniverseListViewControl_UniverseScrollbar
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            usVerticalScrollbar = new Scrollbar.UniverseScrollbar();
            ulvMain = new UniverseListViewControl();
            SuspendLayout();
            // 
            // usVerticalScrollbar
            // 
            usVerticalScrollbar.ActiveBackColor = System.Drawing.Color.Gray;
            usVerticalScrollbar.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            usVerticalScrollbar.LargeChange = 10;
            usVerticalScrollbar.Location = new System.Drawing.Point(268, 0);
            usVerticalScrollbar.Maximum = 99;
            usVerticalScrollbar.Minimum = 0;
            usVerticalScrollbar.Name = "usVerticalScrollbar";
            usVerticalScrollbar.Size = new System.Drawing.Size(29, 397);
            usVerticalScrollbar.SmallChange = 1;
            usVerticalScrollbar.TabIndex = 1;
            usVerticalScrollbar.Text = "universeScrollbar1";
            usVerticalScrollbar.ThumbStyle = Scrollbar.UniverseScrollbar.ThumbStyleEnum.Auto;
            usVerticalScrollbar.Value = 0;
            // 
            // ulvMain
            // 
            ulvMain.Dock = System.Windows.Forms.DockStyle.Fill;
            ulvMain.HideSelection = false;
            ulvMain.Location = new System.Drawing.Point(0, 0);
            ulvMain.Name = "ulvMain";
            ulvMain.Size = new System.Drawing.Size(300, 400);
            ulvMain.TabIndex = 2;
            ulvMain.UseCompatibleStateImageBehavior = false;
            ulvMain.View = System.Windows.Forms.View.Details;
            ulvMain.VScrollbar = usVerticalScrollbar;
            // 
            // UniverseListViewControl_UniverseScrollbar
            // 
            Controls.Add(usVerticalScrollbar);
            Controls.Add(ulvMain);
            Name = "UniverseListViewControl_UniverseScrollbar";
            Size = new System.Drawing.Size(300, 400);
            ResumeLayout(false);
        }

        #endregion

        private Scrollbar.UniverseScrollbar usVerticalScrollbar;
        private UniverseListViewControl ulvMain;
    }
}